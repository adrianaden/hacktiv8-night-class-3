$(document).ready(function(){
  var profile = {
    name: 'Adrian Adendrata',
    summary: 'Hello my name is '+name+' but you can call me Aden. I\'m working at Fin-Tech Company in Indonesia as Fullstack Software Engginer. Now, i focus on learing HTML + JavaScript with Hacktiv8',
    skills: 'Java, Java Script, C#, PHP, HTML, Groovy, TypeScript',
    hobbies: 'Programming, Photography',
    favorites: 'Youtube, Facebook, Detik, Stackoverflow',
    loadProfileAsset: function(){
      $('.profile-picture').attr('src','assets/picture/profile-picture.png')
      $('.cover-photo').css({
        'background-image': 'url("https://timdorr.com/press/wp-content/uploads/2016/01/Code_feat.jpg")'
      })
    },
    loadProfile: function() {
      $("#profile-name").text(this.name)
      $("#profile-summary").text(this.summary)
      $("#profile-skills").text(this.skills)
      $("#profile-hobbies").text(this.hobbies)
      $("#profile-favorites").text(this.favorites)
      this.loadProfileAsset();
    }
  }

  var friends = [{
      name: "Anna",
      job: "Programmer",
      photo: "https://randomuser.me/api/portraits/women/60.jpg",
    },{
      name: "Thomas",
      job: "Lead Marketing",
      photo: "https://randomuser.me/api/portraits/men/60.jpg",
    },{
      name: "Sisca",
      job: "Senior Programmer",
      photo: "https://randomuser.me/api/portraits/women/61.jpg",
    },
  ]
  profile.loadProfile();
  loadFriends(friends)

  function loadFriends(friends){
      friends.forEach(function(value){
        console.log(value.name);
        var htmlFriend = `
        <div class="row friend">
          <div class="col-lg-4">
            <img class="friend-picture" src="${value.photo}"></img>
          </div>
          <div class="col-lg-8">
            <strong>${value.name}</strong>
            <p>${value.job}<p>
          </div>
        </div>
        `
        $('#friends').append(htmlFriend)
    })
  };
  $('#submit-comment').on('click', function() {
    var comment = $('#input-comment').val();
    $('#comment').append(`<p>${comment}</p><hr>`)
    $('#input-comment').val('');
  });
})
